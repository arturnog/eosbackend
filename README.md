## Backend for an EOS (Environmental Observing Station)

This project consists in the backend for an EOS. An EOS is an embedded system used for collecting environmental data. It consists of a computing platform (Raspberry Pi) and several sensors attached. The collected data on each EOS is sent to this server.

This application is a Java EE project. It consists of a REST API and a persistence layer for database interaction. Currently, we use MongoDB for storing the timeseries data. This server supports a frontend application developed in Angular.

Information stored in the database:

 * Timestamp
 * Value
 * Description (Optional)
 
REST API endpoints:

* GET /get - retrieve all dump records
* GET /get/{id} - retrieve specific dump record
* POST / - post a new dump record

### Build the project

To build the project, run the following command:

`mvn clean package`

## Deployment

The generated artifact needs to be deployed in an application server. We use Payara Server/Micro version 5.183.

###Docker

Using the **Dockerfile**:

1. Build the image
    * `docker build -t payara-backend .`
2. Run the image
    * `docker run -p 8080:8080 payara-eosbackend`
    
Using **Docker-compose**:

1. Build and run the environment
    * `docker-compose up`
    
2. Stop and destroy the environment
    * `docker-compose down`
    
Note*: The Dockerfile builds an image with only Payara-micro. The docker-compose builds a Payara-micro and a MongoDB environment.