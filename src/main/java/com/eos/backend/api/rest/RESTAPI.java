package com.eos.backend.api.rest;

import com.eos.backend.domain.dto.DumpDTO;
import com.eos.backend.domain.services.DumpService;
import com.eos.backend.domain.services.impl.DumpServiceImpl;

import javax.inject.Inject;
import javax.json.Json;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.logging.Logger;

@Path("api/v1/")
public class RESTAPI {

    private static Logger logger = Logger.getLogger(RESTAPI.class.getName());

    @Inject
    private DumpService dumpService;

    /**
     * Returns the list of dumps in the db.
     * @return list of dumps retrieved from the bd. empty list if no dump records found.
     */
    @GET
    @Path("dump/get/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDumps(){
        List<DumpDTO> data = dumpService.getDumps();
        logger.info("DATA: " + data.toString());
        return Response.ok().entity(data).build();
    }

    /**
     * Returns the dump from db specified by the id.
     * @return the dump with the specified id.
     */
    @GET
    @Path("dump/get/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDump(@PathParam("id") final String id){
        DumpDTO dto = dumpService.getDump(id);
        logger.info("DTO found: " + dto.toString());
        return Response.ok().entity(dto).build();
    }

    /**
     * Add the dump in the db.
     * @param dumpDTO the DTO of the record to be added.
     * @return the id of the added dump record.
     */
    @POST
    @Path("dump/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response postDump(final DumpDTO dumpDTO){
        logger.info("Posting data to server: " + dumpDTO.toString());
        String id = dumpService.createDump(dumpDTO);
        return Response.ok().entity(
                        Json.createObjectBuilder().add("id",id).build()
                ).build();
    }
}
