package com.eos.backend.domain.dao;

import java.util.List;
import java.util.Optional;

/**
 * Generic interface for Data Access Object Pattern.
 * To be implemented by each resource service which interacts with the db.
 * @param <T> entity
 */
public interface DAO<T> {

    /**
     *
     * @param t the DTO to be inserted in the db.
     * @return returns null if failed to create record; otherwise, returns ID of inserted record.
     */
    Optional<String> create(final T t);

    /**
     *
     * @param t the DTO to be updated in the db.
     * @return returns null if failed to update; otherwise, returns the DTO of the added entity.
     */
    Optional<T> update(final T t);

    /**
     *
     * @param id the id to search entity in the db.
     * @return return the element found; null if not found.
     */
    Optional<T> get(final String id);

    /**
     *
     * @return list of all entities found in the db.
     */
    List<T> getAll();

    /**
     *
     * @param t entity to check if exists
     * @return boolean of whether the entity exists or not in the bd.
     */
    boolean exists(final T t);

    /**
     *
     * @param id remove the entity from db with the specified id.
     */
    void remove(final String id);
}
