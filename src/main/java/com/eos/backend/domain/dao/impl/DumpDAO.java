package com.eos.backend.domain.dao.impl;

import com.eos.backend.domain.dao.DAO;
import com.eos.backend.domain.dto.DumpDTO;
import com.eos.backend.domain.entities.Dump;
import com.eos.backend.domain.exceptions.ApplicationFailedException;
import com.eos.backend.domain.utils.mappers.DumpMapper;
import com.eos.backend.infrastructure.database.MongoDbConnection;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import org.bson.types.ObjectId;
import xyz.morphia.Datastore;
import xyz.morphia.Key;
import xyz.morphia.query.Query;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Logger;

/**
 * DumpDAO class.
 * Receives DumpDTO from service and interacts with the db.
 * Map DTO to Entity.
 */
public class DumpDAO implements DAO<DumpDTO> {

    public static final String TABLE_NAME = "dump";
    private Logger logger = Logger.getLogger(getClass().getName());
    private Gson gson = new Gson(); //to map objects to json and vice-versa

    @MongoDbConnection.DefaultDbConnection
    private MongoDbConnection dbConnection;

    @Inject
    public DumpDAO(final MongoDbConnection dbConnection){
        this.dbConnection=dbConnection;
    }

    @Override
    public Optional<String> create(final DumpDTO dumpDTO) throws ApplicationFailedException{
        Datastore datastore = dbConnection.getDatastore();

        Dump dump = DumpMapper.DumpDTOtoEntity(dumpDTO);
        Key<Dump> key = datastore.save(dump);

        if(!Optional.ofNullable(key).isPresent()){
            throw new ApplicationFailedException("Failed to create Dump" + dumpDTO.toString() + "!");
        }

        String id = key.getId().toString();
        logger.info("Added " + dumpDTO.toString() + " to the db. key:" + id);
        return Optional.ofNullable(id);
    }

    @Override
    public void remove(final String id) throws ApplicationFailedException {
        Datastore datastore = dbConnection.getDatastore();

        Query<Dump> query = datastore
                .createQuery(Dump.class)
                .field("_id")
                .equal(id);

        Dump dump = datastore.findAndDelete(query);
        if(!Optional.ofNullable(dump).isPresent()){
            logger.severe("Failed removing record with ID: " + id);
            throw new ApplicationFailedException("Cant find Dump in the db with ID: " + id + "!");
        }
        logger.info("Removed " + dump.toString() + " from database.");
    }

    @Override
    public Optional<DumpDTO> update(final DumpDTO dumpDTO) throws ApplicationFailedException{
        Datastore datastore = dbConnection.getDatastore();

        Dump dump = datastore
                .createQuery(Dump.class)
                .field("_id")
                .equal(dumpDTO.getId())
                .get();

        if(!Optional.ofNullable(dump).isPresent()){
            throw new ApplicationFailedException("Cant update Dump " + dumpDTO.toString() + "! Not found in the db!");
        }

        Dump newDump = DumpMapper.DumpDTOtoEntity(dumpDTO);

        Key<Dump> key = datastore.save(newDump);

        if(!Optional.ofNullable(key).isPresent()){
            throw new ApplicationFailedException("Failed to update Dump " + "!");
        }
        Optional<DumpDTO> dto = Optional.of(DumpMapper.DumpEntityToDTO(newDump));
        return dto;
    }

    @Override
    public Optional<DumpDTO> get(final String id) throws ApplicationFailedException{
        Datastore datastore = dbConnection.getDatastore();

        Dump dump = findById(datastore, id);

        if(!Optional.ofNullable(dump).isPresent()){
            throw new ApplicationFailedException("Cant find Dump in the db with ID: " + id + "!");
        }
        Optional<DumpDTO> dto = Optional.of(DumpMapper.DumpEntityToDTO(dump));
        return dto;
    }

    @Override
    public List<DumpDTO> getAll() throws ApplicationFailedException{
        Datastore datastore = dbConnection.getDatastore();

        List<DumpDTO> dumps = new ArrayList<>();
        DBCollection dumpCollection = datastore.getCollection(Dump.class);
        DBCursor cursor = dumpCollection.find().sort(new BasicDBObject("timestamp",-1)); //order by timestamp desc

        Iterator<DBObject> it = cursor.iterator();
        while(it.hasNext()){
            DBObject dbobj = it.next();

            DumpDTO dumpDTO = DumpMapper.DumpEntityToDTO(toDump(dbobj));
            dumps.add(dumpDTO);
        }
        cursor.close();
        logger.info("DUMPS: " + dumps.toString());
        return dumps;
    }

    @Override
    public boolean exists(final DumpDTO dumpDTO) throws ApplicationFailedException{
        Dump dump = DumpMapper.DumpDTOtoEntity(dumpDTO);

        Datastore datastore = dbConnection.getDatastore();
        Dump dp = datastore.find(Dump.class)
                .filter("_id =",dump.getId())
                .filter("value =",dump.getValue())
                .filter("timestamp =",dump.getTimestamp())
                .filter("description =",dump.getDescription())
                .get();

        if(!Optional.ofNullable(dp).isPresent())
            return false;
        return true;
    }

    private Dump findById(final Datastore datastore, final String id){
        ObjectId objId;
        try{
            objId = new ObjectId(id);
        }
        catch (IllegalArgumentException e){
            throw new ApplicationFailedException(e.getMessage());
        }

        Dump dump = datastore
                .find(Dump.class)
                .filter("_id =", objId)
                .get();

        return dump;
    }

    private Dump toDump(final DBObject dbObj) throws ApplicationFailedException {
        Dump dump=null;
        Map<String, Object> map = dbObj.toMap();
        logger.info("Value: " + map.toString());

        ObjectId id = (ObjectId) map.get("_id");
        Date date = (Date) map.get("timestamp");
        Timestamp ts = new Timestamp(date.getTime());
        String value = (String) map.get("value");
        String description = null;
        if(map.containsKey("description")){
            description = (String) map.get("description");
        }
        dump = new Dump.Builder(ts,value)
                .id(id)
                .description(description)
                .build();
        return dump;
    }
}
