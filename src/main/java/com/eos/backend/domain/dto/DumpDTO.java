package com.eos.backend.domain.dto;

/**
 * DumpDTO
 */
public class DumpDTO {

    private String id;
    private String timestamp;
    private String value;
    private String description;

    public DumpDTO(){

    }

    public void setTimestamp(final String t){
        this.timestamp=t;
    }

    public void setValue(final String v){
        this.value=v;
    }

    public void setDescription(final String d){
        this.description=d;
    }

    public void setId(final String id){
        this.id=id;
    }

    public String getTimestamp(){
        return timestamp;
    }

    public String getValue(){
        return value;
    }

    public String getDescription(){
        return description;
    }

    public String getId(){
        return id;
    }

    @Override
    public String toString(){
        return "[" + timestamp + "," + value + "," + description + "]";
    }
}
