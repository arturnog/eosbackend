package com.eos.backend.domain.entities;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.bson.types.ObjectId;
import xyz.morphia.annotations.Entity;
import xyz.morphia.annotations.Id;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
@NoArgsConstructor
@Entity("dump")
public class Dump implements Serializable {

    @Id
    private ObjectId id;
    @NonNull
    private Timestamp timestamp;
    @NonNull
    private String value;
    private String description;

    public Dump(final Builder builder){
        this.id=builder.id;
        this.timestamp=builder.timestamp;
        this.value=builder.value;
        this.description=builder.description;
    }

    public static class Builder{

        private final Timestamp timestamp;
        private final String value;
        private ObjectId id;
        private String description;

        public Builder(final Timestamp ts, final String value){
            this.timestamp=ts;
            this.value=value;
        }

        public Builder description(final String descr){
            if(descr!=null)
                this.description=descr;
            return this;
        }

        public Builder id(final String id){
            if(id!=null)
                this.id = new ObjectId(id);
            return this;
        }

        public Builder id(final ObjectId id){
            if(id!=null)
                this.id = id;
            return this;
        }

        public Dump build(){
            return new Dump(this);
        }
    }

    public Timestamp getTimestamp(){
        return timestamp;
    }

    public String getValue(){
        return value;
    }

    public String getDescription(){
        return description;
    }

    public ObjectId getId(){
        return id;
    }
}
