package com.eos.backend.domain.exceptions;


import javax.ejb.ApplicationException;
import java.util.logging.Logger;

/**
 * Custom exception to be used throughout the application.
 */
@ApplicationException
public class ApplicationFailedException extends RuntimeException {

    private static final long serialVersionUID = -3886294039670535105L;
    private Logger logger = Logger.getLogger(getClass().getName());

    public ApplicationFailedException(String msg, Throwable error){
        super(msg, error);
        logger.severe("ApplicationFailedException: " + msg + ". Throwable: " + error.getMessage());
    }

    public ApplicationFailedException(String msg){
        super(msg);
        logger.severe("ApplicationFailedException: " + msg);
    }
}
