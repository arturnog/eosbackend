package com.eos.backend.domain.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

/**
 * Map an ApplicationFailedException to a HTTP Response.
 * Triggered whenever an ApplicationFailedException is thrown.
 */
@Provider
public class ApplicationFailedMapper
        implements ExceptionMapper<ApplicationFailedException> {

    @Override
    public Response toResponse(ApplicationFailedException e) {
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(e.getMessage())
                .build();
    }
}