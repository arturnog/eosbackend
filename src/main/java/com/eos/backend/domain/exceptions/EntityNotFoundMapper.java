package com.eos.backend.domain.exceptions;

import javax.persistence.EntityNotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

/**
 * Exception to entity not found.
 * Useful when DTO to Entity fails or db fails to map to entity.
 */
@Provider
public class EntityNotFoundMapper
        implements ExceptionMapper<EntityNotFoundException> {

    @Override
    public Response toResponse(EntityNotFoundException e) {
        return Response.status(Response.Status.NOT_FOUND).build();
    }
}
