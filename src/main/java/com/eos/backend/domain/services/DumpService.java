package com.eos.backend.domain.services;

import com.eos.backend.domain.dto.DumpDTO;
import com.eos.backend.domain.exceptions.ApplicationFailedException;

import java.util.List;

public interface DumpService {

    String createDump(final DumpDTO dumpDto) throws ApplicationFailedException;

    List<DumpDTO> getDumps();

    DumpDTO getDump(final String id) throws ApplicationFailedException;

    void removeDump(final DumpDTO dumpDto);
}
