package com.eos.backend.domain.services.impl;

import com.eos.backend.domain.dao.impl.DumpDAO;
import com.eos.backend.domain.dto.DumpDTO;
import com.eos.backend.domain.exceptions.ApplicationFailedException;
import com.eos.backend.domain.services.DumpService;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

public class DumpServiceImpl implements DumpService {

    private static Logger logger = Logger.getLogger(DumpServiceImpl.class.getName());
    private DumpDAO dumpDao;

    @Inject
    public DumpServiceImpl(final DumpDAO dumpDAO){
        this.dumpDao=dumpDAO;
    }

    @Override
    public String createDump(final DumpDTO dumpDto) throws ApplicationFailedException{
        logger.info("Adding Dump: " + dumpDto.toString());

        Optional<String> id = dumpDao.create(dumpDto);
        if(!id.isPresent()){
            throw new ApplicationFailedException(dumpDto.toString() + " not added to DB!");
        }
        return id.get();
    }

    @Override
    public List<DumpDTO> getDumps(){
        List<DumpDTO> dumpDTOs = dumpDao.getAll();
        logger.info(dumpDTOs.toString());
        return dumpDTOs;
    }

    @Override
    public DumpDTO getDump(final String id) throws ApplicationFailedException{
        logger.info("Getting Dump with ID: " + id);

        Optional<DumpDTO> dto = dumpDao.get(id);
        if(!dto.isPresent()){
            throw new ApplicationFailedException("Failed getting Dump with ID: " + id);
        }
        return dto.get();
    }

    @Override
    public void removeDump(final DumpDTO dumpDto){
        logger.info("Removing DumpDTO: " + dumpDto.toString());
        dumpDao.remove(dumpDto.getId());
    }
}
