package com.eos.backend.domain.utils.mappers;

import com.eos.backend.domain.dto.DumpDTO;
import com.eos.backend.domain.entities.Dump;
import com.eos.backend.domain.exceptions.ApplicationFailedException;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Mappers of Dump.
 * Convert entity to DTO and vice-versa.
 */
public class DumpMapper {

    public static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private DumpMapper(){

    }

    public static DumpDTO DumpEntityToDTO(final Dump dump){
        DumpDTO dumpDTO = new DumpDTO();
        if(dump.getId()!=null)
            dumpDTO.setId(dump.getId().toHexString());
        dumpDTO.setTimestamp(dump.getTimestamp().toString());
        dumpDTO.setValue(dump.getValue());
        if(dump.getDescription()!=null)
            dumpDTO.setDescription(dump.getDescription());
        return dumpDTO;
    }

    public static Dump DumpDTOtoEntity(final DumpDTO dumpDto){
        Dump dump = new Dump.Builder(convertTimestampFromString(dumpDto.getTimestamp()),dumpDto.getValue())
                .description(dumpDto.getDescription())
                .id(dumpDto.getId())
                .build();
        return dump;
    }

    public static Timestamp convertTimestampFromString(final String ts)throws ApplicationFailedException{
        Timestamp timestamp=null;
        try {
            Date parsedDate = dateFormat.parse(ts);
            timestamp= new Timestamp(parsedDate.getTime());
        } catch (ParseException e) {
            throw new ApplicationFailedException("Failed to convert timestamp string! [" + ts + "]");
        }
        return timestamp;
    }

    public static Timestamp getCurrentTimestamp() {
        Date date = new Date();
        Timestamp ts  = new Timestamp(date.getTime());
        try {
            Date parsedDate = dateFormat.parse(ts.toString());
            ts = new Timestamp(parsedDate.getTime());
        } catch (ParseException e) {
            throw new ApplicationFailedException(e.getMessage());
        }
        return ts;
    }
}