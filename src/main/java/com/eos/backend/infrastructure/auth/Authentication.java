package com.eos.backend.infrastructure.auth;

public interface Authentication {

    public boolean isAuthenticated();
}
