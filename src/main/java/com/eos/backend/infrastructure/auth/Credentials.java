package com.eos.backend.infrastructure.auth;

import lombok.Data;
import lombok.NonNull;

@Data
public class Credentials implements Authentication{

    @NonNull
    private String username;
    @NonNull
    private String password;

    public Credentials(String username, String password){
        this.username=username;
        this.password=password;
    }

    @Override
    public boolean isAuthenticated(){
        if(username.equals("admin") && password.equals("admin")){
            return true;
        }
        return false;
    }
}
