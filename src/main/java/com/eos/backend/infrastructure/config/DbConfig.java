package com.eos.backend.infrastructure.config;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * Singleton class that reads the db config file and loads its properties.
 */
@Singleton
@Startup
public class DbConfig {

    private static String host;
    private static String database;
    private static int port;
    private static String entitiesPackage;
    private InputStream inputStream;
    private String fileName = "db.config";
    private Logger logger = Logger.getLogger(DbConfig.class.getName());

    @Inject
    public DbConfig(){

    }

    @PostConstruct
    private void setConfigProperties(){
        try{
            Properties prop = new Properties();
            inputStream = getClass().getClassLoader().getResourceAsStream(fileName);
            if(inputStream!=null){
                prop.load(inputStream);
            }
            else{
                throw new FileNotFoundException(fileName + " not found!");
            }
            host = prop.getProperty("host");
            database = prop.getProperty("database");
            port = Integer.valueOf(prop.getProperty("port"));
            entitiesPackage = prop.getProperty("entitiespackage");
            inputStream.close();
        }
        catch(IOException e){
            logger.severe("Failed to load " + fileName + " configuration file.");
        }
    }

    public static String getHost(){
        return host;
    }

    public static String getDatabase(){
        return database;
    }

    public static int getPort(){
        return port;
    }

    public static String getEntitiesPackage(){
        return entitiesPackage;
    }
}
