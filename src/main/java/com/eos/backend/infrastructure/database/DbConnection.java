package com.eos.backend.infrastructure.database;

public interface DbConnection<T> {

    T getConnection();
}
