package com.eos.backend.infrastructure.database;

import com.eos.backend.infrastructure.config.DbConfig;
import com.mongodb.MongoClient;
import xyz.morphia.Datastore;
import xyz.morphia.Morphia;

import javax.annotation.PostConstruct;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.DependsOn;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.inject.Inject;
import javax.inject.Qualifier;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.util.logging.Logger;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
public class MongoDbConnection implements DbConnection<MongoDbConnection>{

    @Qualifier
    @Target({ TYPE, METHOD, PARAMETER, FIELD })
    @Retention(RUNTIME)
    public @interface DefaultDbConnection {}

    private Logger logger = Logger.getLogger(getClass().getName());
    private MongoClient mongoClient;
    private Morphia morphia;
    private Datastore datastore;

    @Inject
    public MongoDbConnection(){

    }

    @PostConstruct
    public void init(){

        morphia = new Morphia();
        morphia.mapPackage(DbConfig.getEntitiesPackage());
        datastore = morphia.createDatastore(new MongoClient(), DbConfig.getDatabase());
        logger.info("Connected to database " + DbConfig.getDatabase() + " on host " + DbConfig.getHost() + ":" + DbConfig.getPort());
        datastore.ensureIndexes();
    }

    @Lock(LockType.READ)
    public Datastore getDatastore(){
        if(datastore==null){
            init();
        }
        return datastore;
    }

    @Override
    public MongoDbConnection getConnection() {
        return this;
    }
}
