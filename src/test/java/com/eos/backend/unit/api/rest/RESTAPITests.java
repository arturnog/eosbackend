package com.eos.backend.unit.api.rest;

import com.eos.backend.api.rest.RESTAPI;
import com.eos.backend.domain.dto.DumpDTO;
import com.eos.backend.domain.services.DumpService;
import com.eos.backend.domain.utils.mappers.DumpMapper;
import org.bson.types.ObjectId;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RESTAPITests {

    @Mock
    private DumpService dumpService;

    @InjectMocks
    private RESTAPI restapi;

    @Before
    public void initialize(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getDumpsEmptyNoFail(){
        List<DumpDTO> dumps = new ArrayList<>();

        Mockito.when(dumpService.getDumps()).thenReturn(dumps);

        Response response = restapi.getDumps();

        Mockito.verify(dumpService,Mockito.times(1)).getDumps();
        assertEquals(response.getEntity(),dumps);
    }

    @Test
    public void getDumpsNotEmptyNoFail(){
        List<DumpDTO> dumps = new ArrayList<>();
        dumps.add(setDumpDTOAllAttributes());
        dumps.add(setDumpDTOBasicNoDescriptionNoId());
        dumps.add(setDumpDTOAllAttributes());

        Mockito.when(dumpService.getDumps()).thenReturn(dumps);

        Response response = restapi.getDumps();

        Mockito.verify(dumpService,Mockito.times(1)).getDumps();
        assertEquals(response.getEntity(),dumps);
    }

    @Test
    public void getDumpNoFail(){
        String mockId = "12345";
        DumpDTO dto = setDumpDTOBasicNoDescriptionNoId();
        dto.setId(mockId);

        Mockito.when(dumpService.getDump(mockId)).thenReturn(dto);

        Response response = restapi.getDump(mockId);

        Mockito.verify(dumpService,Mockito.times(1)).getDump(mockId);
        assertEquals(response.getEntity(),dto);
    }

    @Test
    public void postDumpNoFail(){
        DumpDTO dto = setDumpDTOAllAttributes();
        String mockId = dto.getId();
        JsonObject jsonObject = Json.createObjectBuilder()
                .add("id",mockId)
                .build();

        Mockito.when(dumpService.createDump(dto)).thenReturn(mockId);

        Response response = restapi.postDump(dto);

        Mockito.verify(dumpService,Mockito.times(1)).createDump(dto);
        assertEquals(response.getEntity(),jsonObject);
    }

    private DumpDTO setDumpDTOBasicNoDescriptionNoId(){
        DumpDTO dto = new DumpDTO();
        dto.setTimestamp(DumpMapper.getCurrentTimestamp().toString());
        dto.setValue("10.5");

        return dto;
    }

    private DumpDTO setDumpDTOAllAttributes(){
        DumpDTO dto = new DumpDTO();
        dto.setTimestamp(DumpMapper.getCurrentTimestamp().toString());
        dto.setValue("11.5");
        dto.setId((new ObjectId()).toHexString());
        dto.setDescription("Description");
        return dto;
    }
}
