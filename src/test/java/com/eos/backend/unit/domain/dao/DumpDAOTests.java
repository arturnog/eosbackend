package com.eos.backend.unit.domain.dao;

import com.eos.backend.domain.dao.impl.DumpDAO;
import com.eos.backend.domain.dto.DumpDTO;
import com.eos.backend.domain.entities.Dump;
import com.eos.backend.domain.exceptions.ApplicationFailedException;
import com.eos.backend.domain.utils.mappers.DumpMapper;
import com.eos.backend.infrastructure.database.MongoDbConnection;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import org.bson.types.ObjectId;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import xyz.morphia.Datastore;
import xyz.morphia.Key;
import xyz.morphia.query.Query;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;

public class DumpDAOTests {

    @Mock
    private MongoDbConnection dbConnection;

    @Mock
    private Datastore datastore;

    @InjectMocks
    private DumpDAO dumpDAO;

    @Before
    public void initialize(){
        MockitoAnnotations.initMocks(this);
        Mockito.when(dbConnection.getDatastore()).thenReturn(datastore);
    }

    @Test
    public void createDumpAllAttributesNoFail(){
        DumpDTO dto = setDumpDTOAllAttributes();
        Dump dump = DumpMapper.DumpDTOtoEntity(dto);

        Key<Dump> keyRes = new Key<>(Dump.class,DumpDAO.TABLE_NAME,dto.getId());

        Mockito.when(dbConnection.getDatastore().save(dump)).thenReturn(keyRes);

        Optional<String> id = dumpDAO.create(dto);

        Mockito.verify(dbConnection.getDatastore(),Mockito.times(1)).save(dump);
        assertEquals(id.get(),dto.getId());
    }

    @Test
    public void createDumpBasicAttributesNoFail(){
        String mockId = (new ObjectId()).toHexString();
        DumpDTO dto = setDumpDTOBasicNoDescriptionNoId();
        Dump dump = DumpMapper.DumpDTOtoEntity(dto);

        Key<Dump> keyRes = new Key<>(Dump.class,DumpDAO.TABLE_NAME,mockId);

        Mockito.when(dbConnection.getDatastore().save(dump)).thenReturn(keyRes);

        Optional<String> id = dumpDAO.create(dto);

        Mockito.verify(dbConnection.getDatastore(),Mockito.times(1)).save(dump);
        assertEquals(id.get(),mockId);
    }

    @Test
    public void createDumpBasicAttributesMissingIdThrowsApplicationFailedException(){
        DumpDTO dto = setDumpDTOBasicNoDescriptionNoId();
        Dump dump = DumpMapper.DumpDTOtoEntity(dto);

        Mockito.when(dbConnection.getDatastore().save(dump)).thenReturn(null);

        Optional<String> id = null;

        try{
            id = dumpDAO.create(dto);

            fail("Should throw an ApplicationFailedException!");
        }
        catch (RuntimeException e){
            Mockito.verify(dbConnection.getDatastore(),Mockito.times(1)).save(dump);
            assertNull(id);
        }
    }

    @Test
    public void createDumpBasicAttributesInvalidTimestampThrowsApplicationFailedException(){
        DumpDTO dto = setDumpDTOBasicNoValidTimestamp();

        Optional<String> id = null;

        try{
            id = dumpDAO.create(dto);

            fail("Should throw an ApplicationFailedException!");
        }
        catch (ApplicationFailedException e){
            Mockito.verify(dbConnection.getDatastore(),Mockito.times(0)).save(anyIterable());
            assertNull(id);
        }
    }

    @Test
    public void removeDumpNoFail(){
        String mockId = (new ObjectId()).toHexString();
        DumpDTO dto = setDumpDTOBasicNoDescriptionNoId();
        dto.setId(mockId);

        Dump dump = DumpMapper.DumpDTOtoEntity(dto);

        Query<Dump> query = Mockito.mock(Query.class, Mockito.RETURNS_DEEP_STUBS);

        Mockito.when(dbConnection.getDatastore().createQuery(Dump.class)).thenReturn(query);
        Mockito.when(dbConnection.getDatastore().findAndDelete(any())).thenReturn(dump);

        dumpDAO.remove(mockId);

        Mockito.verify(dbConnection.getDatastore(),Mockito.times(1)).findAndDelete(any());
        Mockito.verify(dbConnection.getDatastore(),Mockito.times(1)).createQuery(Dump.class);
    }

    @Test
    public void removeDumpThrowsApplicationFailedException() {
        String mockId = (new ObjectId()).toHexString();
        DumpDTO dto = setDumpDTOBasicNoDescriptionNoId();
        dto.setId(mockId);

        Query<Dump> query = Mockito.mock(Query.class, Mockito.RETURNS_DEEP_STUBS);

        Mockito.when(dbConnection.getDatastore().createQuery(Dump.class)).thenReturn(query);
        Mockito.when(dbConnection.getDatastore().findAndDelete(any())).thenReturn(null);

        try{
            dumpDAO.remove(mockId);

            fail("Should throw an ApplicationFailedException!");
        }
        catch (RuntimeException e){
            Mockito.verify(dbConnection.getDatastore(),Mockito.times(1)).findAndDelete(any());
            Mockito.verify(dbConnection.getDatastore(),Mockito.times(1)).createQuery(Dump.class);
        }
    }

    @Test
    public void updateDumpNoFail(){
        DumpDTO dto = setDumpDTOAllAttributes();
        Dump dump = DumpMapper.DumpDTOtoEntity(dto);

        Query<Dump> query = Mockito.mock(Query.class, Mockito.RETURNS_DEEP_STUBS);

        Mockito.when(dbConnection.getDatastore().createQuery(Dump.class)).thenReturn(query);
        Mockito.when(query.field(anyString()).equal(dto.getId()).get()).thenReturn(dump);

        Key<Dump> keyRes = new Key<>(Dump.class,DumpDAO.TABLE_NAME,dump.getId());

        Mockito.when(dbConnection.getDatastore().save(dump)).thenReturn(keyRes);

        Optional<DumpDTO> dtoRes = dumpDAO.update(dto);

        Mockito.verify(dbConnection.getDatastore(),Mockito.times(1)).save(dump);
        assertEquals(dtoRes.isPresent(),true);
        assertEquals(dtoRes.get().getValue(),dto.getValue());
    }

    @Test
    public void updateDumpWithNoIdThrowsNullPointerException(){
        DumpDTO dto = setDumpDTOBasicNoDescriptionNoId();

        Optional<DumpDTO> dtoRes = null;

        try {
            dtoRes = dumpDAO.update(dto);

            fail("Should throw an NullPointerException!");
        }
        catch (NullPointerException e){
            Mockito.verify(dbConnection.getDatastore(),Mockito.times(0)).save(DumpMapper.DumpDTOtoEntity(dto));
            assertNull(dtoRes);
        }
    }

    @Test
    public void updateDumpCantFindRecordThrowsApplicationFailedException(){
        DumpDTO dto = setDumpDTOAllAttributes();

        Query<Dump> query = Mockito.mock(Query.class, Mockito.RETURNS_DEEP_STUBS);

        Mockito.when(dbConnection.getDatastore().createQuery(Dump.class)).thenReturn(query);
        Mockito.when(query.field(anyString()).equal(dto.getId()).get()).thenReturn(null);

        Optional<DumpDTO> dtoRes = null;

        try{
            dtoRes = dumpDAO.update(dto);

            fail("Should throw an ApplicationFailedException!");
        }
        catch (RuntimeException e){
            Mockito.verify(query.field(anyString()).equal(dto.getId()),Mockito.times(1)).get();
            Mockito.verify(dbConnection.getDatastore(),Mockito.times(0)).save(DumpMapper.DumpDTOtoEntity(dto));
            assertNull(dtoRes);
        }
    }

    @Test
    public void updateDumpFailedToUpdateThrowsApplicationFailedException(){
        DumpDTO dto = setDumpDTOAllAttributes();
        Dump dump = DumpMapper.DumpDTOtoEntity(dto);

        Query<Dump> query = Mockito.mock(Query.class, Mockito.RETURNS_DEEP_STUBS);

        Mockito.when(dbConnection.getDatastore().createQuery(Dump.class)).thenReturn(query);
        Mockito.when(query.field(anyString()).equal(dto.getId()).get()).thenReturn(dump);
        Mockito.when(dbConnection.getDatastore().save(dump)).thenReturn(null);

        Optional<DumpDTO> dtoRes = null;

        try{
            dtoRes = dumpDAO.update(dto);

            fail("Should throw an ApplicationFailedException!");
        }
        catch (RuntimeException e){
            Mockito.verify(query.field(anyString()).equal(dto.getId()),Mockito.times(1)).get();
            Mockito.verify(dbConnection.getDatastore(),Mockito.times(1)).save(dump);
            assertNull(dtoRes);
        }
    }

    @Test
    public void getDumpNoFail(){
        String mockId = (new ObjectId()).toHexString();

        Dump dump = setDumpMockWithId(mockId);
        DumpDTO dto = DumpMapper.DumpEntityToDTO(dump);

        Query<Dump> query = Mockito.mock(Query.class, Mockito.RETURNS_DEEP_STUBS);

        Mockito.when(dbConnection.getDatastore().find(Dump.class)).thenReturn(query);
        Mockito.when(query.filter(anyString(),any()).get()).thenReturn(dump);

        Optional<DumpDTO> dtoRes = dumpDAO.get(mockId);

        Mockito.verify(dbConnection.getDatastore(),Mockito.times(1)).find(Dump.class);
        assertEquals(dtoRes.isPresent(),true);
        assertEquals(dtoRes.get().getId(),dto.getId());
        assertEquals(dtoRes.get().getDescription(),dto.getDescription());
        assertEquals(dtoRes.get().getTimestamp(),dto.getTimestamp());
        assertEquals(dtoRes.get().getValue(),dto.getValue());
    }

    @Test
    public void getDumpNotFoundThrowsApplicationFailedException(){
        String mockId = (new ObjectId()).toHexString();

        Dump dump = setDumpMockWithId(mockId);
        DumpDTO dto = DumpMapper.DumpEntityToDTO(dump);

        Query<Dump> query = Mockito.mock(Query.class, Mockito.RETURNS_DEEP_STUBS);

        Mockito.when(dbConnection.getDatastore().find(Dump.class)).thenReturn(query);
        Mockito.when(query.filter(anyString(),any()).get()).thenReturn(null);

        Optional<DumpDTO> dtoRes = null;

        try{
            dtoRes = dumpDAO.get(mockId);

            fail("Should throw an ApplicationFailedException!");
        }
        catch (RuntimeException e){
            Mockito.verify(dbConnection.getDatastore(),Mockito.times(1)).find(Dump.class);
            Mockito.verify(query.filter(anyString(),any()),Mockito.times(1)).get();
            assertNull(dtoRes);
        }
    }

    @Test
    public void getDumpInvalidIdThrowsApplicationFailedException(){
        String mockId = "someIdNotValid";

        Query<Dump> query = Mockito.mock(Query.class, Mockito.RETURNS_DEEP_STUBS);

        Mockito.when(dbConnection.getDatastore().find(Dump.class)).thenReturn(query);

        Optional<DumpDTO> dtoRes = null;

        try{
            dtoRes = dumpDAO.get(mockId);

            fail("Should throw an ApplicationFailedException!");
        }
        catch (RuntimeException e){
            Mockito.verify(dbConnection.getDatastore(),Mockito.times(0)).find(Dump.class);
            Mockito.verify(query.filter(anyString(),any()),Mockito.times(0)).get();
            assertNull(dtoRes);
        }
    }

    @Test
    public void getDumpsNoFail(){
        DBCollection dbCollection = Mockito.mock(DBCollection.class);
        DBCursor dbCursor = Mockito.mock(DBCursor.class);
        Iterator<DBObject> it = Mockito.mock(Iterator.class);

        Mockito.when(dbConnection.getDatastore().getCollection(Dump.class)).thenReturn(dbCollection);
        Mockito.when(dbCollection.find()).thenReturn(dbCursor);
        Mockito.when(dbCollection.find().sort(any())).thenReturn(dbCursor);
        Mockito.when(dbCursor.iterator()).thenReturn(it);
        Mockito.when(it.hasNext()).thenReturn(false);

        List<DumpDTO> dtoListResult = dumpDAO.getAll();

        Mockito.verify(dbConnection.getDatastore(),Mockito.times(1)).getCollection(Dump.class);
        Mockito.verify(dbCollection.find(),Mockito.times(1)).sort(any());
        assertNotNull(dtoListResult);
        assertEquals(dtoListResult.size(),0);
    }

    private DumpDTO setDumpDTOBasicNoDescriptionNoId(){
        DumpDTO dto = new DumpDTO();
        dto.setTimestamp(DumpMapper.getCurrentTimestamp().toString());
        dto.setValue("10.5");
        return dto;
    }

    private DumpDTO setDumpDTOBasicNoValidTimestamp(){
        DumpDTO dto = new DumpDTO();
        dto.setTimestamp("noValidTimestamp");
        dto.setValue("10.5");
        return dto;
    }

    private DumpDTO setDumpDTOAllAttributes(){
        DumpDTO dto = new DumpDTO();
        dto.setTimestamp(DumpMapper.getCurrentTimestamp().toString());
        dto.setValue("11.5");
        dto.setId((new ObjectId()).toHexString());
        dto.setDescription("Description");
        return dto;
    }

    private Dump setDumpMock(){
        Dump dump = new Dump.Builder(DumpMapper.getCurrentTimestamp(),"1")
                .build();

        return dump;
    }

    private Dump setDumpMockWithId(final String id){
        Dump dump = new Dump.Builder(DumpMapper.getCurrentTimestamp(),"1")
                .id(id)
                .build();

        return dump;
    }
}
