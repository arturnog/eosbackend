package com.eos.backend.unit.domain.services;

import com.eos.backend.domain.dao.impl.DumpDAO;
import com.eos.backend.domain.dto.DumpDTO;
import com.eos.backend.domain.services.impl.DumpServiceImpl;
import com.eos.backend.domain.utils.mappers.DumpMapper;
import org.bson.types.ObjectId;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DumpServiceTests {

    @Mock
    private DumpDAO dumpDAO;

    @InjectMocks
    private DumpServiceImpl dumpService;

    @Before
    public void initialize(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void createBasicDumpNoException(){
        DumpDTO dto = setDumpDTOBasicNoDescriptionNoId();
        Optional<String> mockId = Optional.of( "12345");

        when(dumpDAO.create(dto)).thenReturn(mockId);

        String id = dumpService.createDump(dto);

        verify(dumpDAO, Mockito.times(1)).create(dto);
        assertEquals(id,mockId.get());
    }

    @Test
    public void createBasicDumpThrowsApplicationFailedException(){
        DumpDTO dto = setDumpDTOBasicNoDescriptionNoId();
        Optional<String> mockId = Optional.ofNullable(null);

        when(dumpDAO.create(dto)).thenReturn(mockId);

        String id = null;

        try{
            id = dumpService.createDump(dto);

            fail("Should throw an ApplicationFailedException!");
        }
        catch (RuntimeException e){
            verify(dumpDAO, Mockito.times(1)).create(dto);
            assertEquals(id,null);
        }
    }

    @Test
    public void getDumpNoException(){
        DumpDTO dto = setDumpDTOBasicNoDescriptionNoId();
        String mockId = "12345";
        dto.setId(mockId);

        Optional<DumpDTO> dtoReturn = Optional.of(dto);

        when(dumpDAO.get(mockId)).thenReturn(dtoReturn);

        DumpDTO dtoResult = dumpService.getDump(mockId);

        verify(dumpDAO, Mockito.times(1)).get(mockId);
        assertEquals(dtoResult,dtoReturn.get());
    }

    @Test
    public void getDumpThrowsApplicationFailedException(){
        String mockId = "12345";

        Optional<DumpDTO> dtoReturn = Optional.ofNullable(null);

        when(dumpDAO.get(mockId)).thenReturn(dtoReturn);

        DumpDTO dtoResult = null;

        try{
            dtoResult = dumpService.getDump(mockId);

            fail("Should throw an ApplicationFailedException!");
        }
        catch (RuntimeException e){
            verify(dumpDAO, Mockito.times(1)).get(mockId);
            assertEquals(dtoResult,null);
        }
    }

    @Test
    public void getAllDumpsNoException(){
        List<DumpDTO> dtoListReturn = new ArrayList<>();
        dtoListReturn.add(setDumpDTOAllAttributes());
        dtoListReturn.add(setDumpDTOBasicNoDescriptionNoId());

        when(dumpDAO.getAll()).thenReturn(dtoListReturn);

        List<DumpDTO> dtoListResult = dumpService.getDumps();

        verify(dumpDAO, Mockito.times(1)).getAll();
        assertEquals(dtoListResult,dtoListReturn);
    }

    @Test
    public void removeDumpNoException(){
        DumpDTO dto = setDumpDTOAllAttributes();

        dumpService.removeDump(dto);

        verify(dumpDAO,Mockito.times(1)).remove(dto.getId());
    }

    private DumpDTO setDumpDTOBasicNoDescriptionNoId(){
        DumpDTO dto = new DumpDTO();
        dto.setTimestamp(DumpMapper.getCurrentTimestamp().toString());
        dto.setValue("10.5");

        return dto;
    }

    private DumpDTO setDumpDTOAllAttributes(){
        DumpDTO dto = new DumpDTO();
        dto.setTimestamp(DumpMapper.getCurrentTimestamp().toString());
        dto.setValue("11.5");
        dto.setId((new ObjectId()).toHexString());
        dto.setDescription("Description");
        return dto;
    }
}
