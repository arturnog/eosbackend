package com.eos.backend.unit.domain.utils.mappers;

import com.eos.backend.domain.dto.DumpDTO;
import com.eos.backend.domain.entities.Dump;
import com.eos.backend.domain.utils.mappers.DumpMapper;
import org.bson.types.ObjectId;
import org.junit.Test;

import static com.eos.backend.domain.utils.mappers.DumpMapper.getCurrentTimestamp;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class DumpMapperTests {

    @Test
    public void DumpEntityToDTOAllAttributesNoFail(){
        Dump dump = setDumpMockAllAttributes();

        DumpDTO dumpDTO = DumpMapper.DumpEntityToDTO(dump);

        assertEquals(dumpDTO.getTimestamp(),dump.getTimestamp().toString());
        assertEquals(dumpDTO.getValue(),dump.getValue());
        assertEquals(dumpDTO.getDescription(),dump.getDescription());
        assertEquals(dumpDTO.getId(),dump.getId().toHexString());
    }

    @Test
    public void DumpEntityToDTOBasicNoFail(){
        Dump dump = setDumpMockBasicNoDescriptionNoId();

        DumpDTO dumpDTO = DumpMapper.DumpEntityToDTO(dump);

        assertEquals(dumpDTO.getTimestamp(),dump.getTimestamp().toString());
        assertEquals(dumpDTO.getValue(),dump.getValue());
        assertEquals(dumpDTO.getDescription(),null);
        assertEquals(dumpDTO.getId(),null);
        assertEquals(dump.getId(),null);
        assertEquals(dump.getDescription(),dumpDTO.getDescription());
    }

    @Test
    public void DumpEntityToDTONoIdNoFail(){
        Dump dump = setDumpMockNoId();

        DumpDTO dumpDTO = DumpMapper.DumpEntityToDTO(dump);

        assertEquals(dumpDTO.getTimestamp(),dump.getTimestamp().toString());
        assertEquals(dumpDTO.getValue(),dump.getValue());
        assertEquals(dumpDTO.getDescription(),dump.getDescription());
        assertEquals(dumpDTO.getId(),null);
    }

    @Test
    public void DumpEntityToDTONoDescriptionNoFail(){
        Dump dump = setDumpMockNoDescription();

        DumpDTO dumpDTO = DumpMapper.DumpEntityToDTO(dump);

        assertEquals(dumpDTO.getTimestamp(),dump.getTimestamp().toString());
        assertEquals(dumpDTO.getValue(),dump.getValue());
        assertEquals(dumpDTO.getDescription(),null);
    }

    @Test
    public void DumpDTOtoEntityAllAttributesNoFail(){
        DumpDTO dto = setDumpDTOAllAttributes();

        Dump dump = DumpMapper.DumpDTOtoEntity(dto);

        assertEquals(dump.getValue(),dto.getValue());
        assertEquals(dump.getTimestamp().toString(),dto.getTimestamp());
        assertEquals(dump.getId().toHexString(),dto.getId());
        assertEquals(dump.getDescription(),dto.getDescription());
    }

    @Test
    public void DumpDTOtoEntityBasicNoFail(){
        DumpDTO dto = setDumpDTOBasicNoDescriptionNoId();

        Dump dump = DumpMapper.DumpDTOtoEntity(dto);

        assertEquals(dump.getValue(),dto.getValue());
        assertEquals(dump.getTimestamp().toString(),dto.getTimestamp());
        assertEquals(dump.getId(),null);
        assertEquals(dump.getId(),dto.getId());
        assertEquals(dump.getDescription(),null);
        assertEquals(dump.getDescription(),dto.getDescription());
    }

    private Dump setDumpMockAllAttributes(){
        Dump dump = new Dump.Builder(getCurrentTimestamp(),"20.1")
                .description("This is the description")
                .id(new ObjectId())
                .build();
        return dump;
    }

    private Dump setDumpMockNoId(){
        Dump dump = new Dump.Builder(getCurrentTimestamp(),"15.9")
                .description("This is the description")
                .build();
        return dump;
    }

    private Dump setDumpMockNoDescription(){
        Dump dump = new Dump.Builder(getCurrentTimestamp(),"30.008")
                .id(new ObjectId())
                .build();
        return dump;
    }

    private Dump setDumpMockBasicNoDescriptionNoId(){
        Dump dump = new Dump.Builder(getCurrentTimestamp(),"-1")
                .build();
        return dump;
    }

    private DumpDTO setDumpDTOBasicNoDescriptionNoId(){
        DumpDTO dto = new DumpDTO();
        dto.setTimestamp(DumpMapper.getCurrentTimestamp().toString());
        dto.setValue("10.5");

        return dto;
    }

    private DumpDTO setDumpDTOAllAttributes(){
        DumpDTO dto = new DumpDTO();
        dto.setTimestamp(DumpMapper.getCurrentTimestamp().toString());
        dto.setValue("11.5");
        dto.setId((new ObjectId()).toHexString());
        dto.setDescription("Description");
        return dto;
    }
}
